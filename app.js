const express = require('express')
const app = express();
var bodyParser = require('body-parser');
var cors = require('cors');

var routes = require('./routes/index');
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api', routes);

var http = require('http').Server(app);


http.listen(port, function () {
    console.log('Server is Listening on http://localhost:' + port);
});

// app.get('/', (req, res) => res.send('Hello World!'));
// app.get('/test', User.GetAllUsers);
// app.listen(port, () => console.log(`Example app listening on port ${port}!`))