//const db = require('./db');
var MongoClient = require('mongodb').MongoClient;
var _db;
var MONGO_URL = "mongodb://ndx:ndx12345@ds347367.mlab.com:47367/heroku_jcc98gcw";
var dbo = MongoClient.connect(MONGO_URL, function(err, db) {
    if(err)
        throw err;
    console.log("connected to the mongoDB !");
    _db = db.collection('users');;
});

module.exports = {
    Login:Login,
    InsertUser: InsertUser,
    UpdateUser: UpdateUser,
    GetAllUsers: GetAllUsers,
    DeleteUser: DeleteUser
    

}

function Login(req, res, next){
    console.log("Login function Called");
    try{
        console.log("req.body: ", req.body);
        var inputJson = req.body;
        console.log("Input Json", inputJson);
        _db.find(inputJson).toArray((err, items) => {
            console.log(items);
        
            res.status(200).json(items);
            res.end();
        })
         
    }catch(ex){
        console.log("Login Exception: ", ex);
        res.status(500).send(ex);
        res.end();
        return next(ex);
    }
}

function InsertUser(req, res, next){
    console.log("Inser User called");
    try{
        console.log("req.body: ", req.body);
        var inputJson = req.body;
        //{firstname: "divya", description: "learn more than everyone"}
        _db.insert(inputJson, function(err, result) {
            if(err)
                throw err;
            
            console.log("entry saved");
            res.status(200)
                .json(result);
            res.end();
        });
    }catch(ex){
        console.log("InsertUser Exception: ", ex);
        res.status(500).send(ex);
        res.end();
        return next(ex);
    }
};

function UpdateUser(req, res, next){
    console.log("UpdateUser Called");
    try{
        var inputJson = req.body;
        var myquery = {emailid: inputJson.emailid};
        var toUpdateValues = {$set: inputJson };
        _db.update(myquery, toUpdateValues,  function(err, result) {
            if(err)
                throw err;
            console.log('entry updated');
            res.status(200)
                .json(result);
            res.end();
        });
    }catch(ex){
        console.log("UpdateUser Exception: ", ex);
        res.status(500).send(ex);
        res.end();
        return next(ex);
    }
};

//------------------------------------------------Get All Users------------------------------------------------------

function GetAllUsers(req, res, next) {
    console.log("GetAllUsers called");
    try {
        console.log("req.body: ", req.body);
        var inputJson = req.body;
        _db.find(inputJson).toArray((err, items) => {
            console.log(items);
        
            res.status(200).json(items);
            res.end();
        })

        // var cursor = _db.find(inputJson);
        // cursor.each(function(err, doc) {
            
        //     console.log("document found:");
        //     console.log(doc);

        //     res.status(200).json(cursor);
        //     res.end();
        // });
    }
    catch (ex) {
        console.log("Main Exception : " + ex);
        
        res.status(500)
            .send(ex);
        return next(ex); // print the error;
    }


};

function DeleteUser(req, res, next){
    console.log("DeleteUser Called");
    try{
        console.log("req.body", req.body);
        var inputJson = req.body;
        _db.findAndModify({emailid: inputJson.emailid}, [], {remove:true}, function(err, object) {
            if(err)
                throw err;
            console.log("document deleted", object);
        });
    }catch(ex){
        console.log("DeleteUser Exception: ", ex);
        res.status(500)
            .send(ex);
        res.end();
        return next(ex);
    }
}
