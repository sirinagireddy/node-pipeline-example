var express = require('express');
var router = express.Router();

console.log("Router Object Loaded");

var User = require('../code/user')
console.log("User Object Loaded");

//router.get('/', (req, res) => res.send('Hello World!'));

router.get('/GetAllUsers', User.GetAllUsers);
router.post('/InsertUser', User.InsertUser);
router.post('/UpdateUser', User.UpdateUser);
router.post('/Login', User.Login);

router.post('/DeleteUser', User.DeleteUser);
console.log("User Api's Loaded");

module.exports = router;